#!/bin/sh
setup_snapper() {
echo "snapper setup"
    pacman -S --noconfirm snapper
    umount /.snapshots
    rm -r /.snapshots
    snapper --no-dbus -c root create-config /
    btrfs subvolume delete /.snapshots
    mkdir /.snapshots
    mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/cryptroot /.snapshots
    chmod 750 /.snapshots


}
