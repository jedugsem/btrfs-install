#!/bin/sh
add_user() {
    USERNAME=$1
    echo "setting up $USERNAME"
    useradd -m $USERNAME
    usermod -aG audio $USERNAME
    usermod -aG input $USERNAME
    usermod -aG video $USERNAME
    usermod -aG network $USERNAME
    #usermod -aG rtkit $USERNAME
    usermod -aG wheel $USERNAME
    echo "passwd $USERNAME"
    passwd $USERNAME
}
