#!/bin/sh
setup_grub() {
    UUID=$1
    SNAPPER=$2
    SWAP=$3
pacman -S --noconfirm grub os-prober efibootmgr openssl openssl-1.1 nano glibc mkinitcpio
#if $SNAPPER; then
    #	    xbps-install -S grub-btrfs
    #fi
    echo "setup grub"

    sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
    sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux


    #sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=4\"/GRUB_CMDLINE_LINUX_DEFAULT=\"rd.auto=1 loglevel=4\"/g" /etc/default/grub

    echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
 if $SWAP; then   
echo "GRUB_CMDLINE_LINUX_DEFAULT=\"cryptdevice=UUID=${UUID}:cryptroot:allow-discards resume=/dev/mapper/$CRYPT resume_offset=${roffset}\"" >> /etc/default/grub
 else

echo "GRUB_CMDLINE_LINUX_DEFAULT=\"cryptdevice=UUID=${UUID}:cryptroot:allow-discards\"" >> /etc/default/grub
fi  
sed -i 's/HOOKS=(.*)/HOOKS=(base udev autodetect modconf block encrypt keyboard keymap consolefont resume filesystems)/g' /etc/mkinitcpio.conf
    #sed -i 's/block/block encrypt/' /etc/mkinitcpio.conf
    sed -i 's/FILES=()/FILES=(\/crypto_keyfile.bin)/g' /etc/mkinitcpio.conf
    #sed -i 's/consolefont/consolefont resume/' /etc/mkinitcpio.conf
    #echo "install_items+=\" /boot/volume.key /etc/crypttab \"" >> /etc/dracut.conf.d/10-crypt.conf
    mkinitcpio -p linux
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=boot
    cp /boot/efi/EFI/boot/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
    grub-mkconfig -o /boot/grub/grub.cfg

}
