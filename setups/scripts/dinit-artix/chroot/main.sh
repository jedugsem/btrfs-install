#!/bin/sh
. /etc/profile
. /home/installer/vars
. /home/installer/chroot/helpers/fns_user.sh
. /home/installer/chroot/helpers/fns_grub.sh
. /home/installer/chroot/helpers/fns_fstab.sh
. /home/installer/chroot/helpers/fns_snapper.sh
. /home/installer/chroot/helpers/fns_locale.sh
. /home/installer/chroot/helpers/fns_swap.sh

#   FSTAB

LUKS_UUID=$(blkid -s UUID -o value /dev/mapper/$CRYPT)
UUID=$(blkid -s UUID -o value /dev/"$ROOT")
EFI_UUID=$(blkid -s UUID -o value /dev/"$EFI")
fstab 


# Locales

#setup_locales
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
sed -i 's/#en_US/en_US/' /etc/locale.gen
locale-gen
echo "export LANG=\"en_US.UTF-8\"
export LC_COLLATE=\"C\"" >> /etc/locale.conf


echo "$SWAP swap"
if $SWAP; then
    # it exports roffset snd 
    create_swap
fi

if $SNAPPER; then
   setup_snapper
fi

echo "doas setup missing the sudo removal"
echo "permit persist :wheel as root
  permit nopass root as $USERNAME" >> /etc/doas.conf


setup_grub $UUID $SNAPPER


# Usersetup
echo $HOSTNAME > /etc/hostname
echo "127.0.0.1        localhost
 ::1              localhost
 127.0.1.1        ${HOSTNAME}.local  ${HOSTNAME}" > /etc/hosts
echo "usersetup\n"
echo "root password"
passwd
add_user $USERNAME
chsh -s /bin/sh $USERNAME
mkdir /etc/iwd/
echo "# Note: The lines starting with # are ignored. To enable any of the
# configuration options below, remove # from the beginning of a respective line.

# this file is not distributed upstream as of iwd 1.0 version
# It is created as transitional config from latest version
# Please read iwd.config(5), iwd.network(5), iwctl(1), iwmon(1) before setting
# these parameters below

# main.conf format is changed with iwd 1.3 release

#[EAP]
#mtu=1400

#[EAPoL]
#max_4way_handshake_time=5

[General]
EnableNetworkConfiguration=True
UseDefaultInterface=true
#ControlPortOverNL80211=True
#RoamThreshold=-70

[Network]
NameResolvingService=resolvconf
#NameResolvingService=resolvconf

[Scan]
DisablePeriodicScan=true
# DisableRoamingScan=false

#[Blacklist]

#[Rank]

# BandModifier5Ghz=1.0" > /etc/iwd/main.conf 
