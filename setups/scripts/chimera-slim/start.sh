#!/bin/sh
. ./vars
. ./disk.sh
echo "sdx1 as boot and sdx2 as root"

MOUNT_OPTS="noatime,compress=zstd"
CRYPT_NAME="cryptroot"
CRYPT_ITER="2000" #Time needed for brute force in ms
ROOT="/mnt"

create_part $DRIVE $CRYPT_NAME $CRYPT_ITER 

mount_part $DRIVE $CRYPT_NAME $MOUNT_OPTS

chimera-bootstrap -f $ROOT

cp -r ./chroot/* /mnt/tmp/
chimera-chroot /mnt /bin/sh /tmp/main.sh

#umount -R /mnt
#umount /mnt
