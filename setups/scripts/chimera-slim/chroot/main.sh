#!/bin/sh
. /home/installer/vars
LUKS_UUID=$(blkid -s UUID -o value /dev/mapper/$CRYPT_NAME)
UUID=$(blkid -s UUID -o value "$DRIVE"2)
EFI_UUID=$(blkid -s UUID -o value "$DRIVE"128)

echo "fstab" 
fstab 

echo "update and additions"
apk update
apk upgrade --available
apk add linux-stable cryptsetup-scripts chimera-repo-contrib iwd dhcpcd ucode-amd

echo "creating swap"
create_swap

roffset=$(btrfs inspect-internal map-swapfile -r /var/swap/swapfile)

echo "setting up snapper"
setup_snapper

echo "now grub for the win"
setup_grub $UUID $SNAPPER

echo "Post Installation"
echo $HOSTNAME > /etc/hostname
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
echo "root password"
passwd
useradd $USERNAME
usermod -a -G wheel,kvm,network $USERNAME
passwd $USERNAME

