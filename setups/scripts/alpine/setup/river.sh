apk add qutebrowser rustup alacritty ttf-font-awesome xwayland zig wlroots swaybg py3-pywal wayland wayland-protocols libxkbcommon libevdev pixman zathura imv brightnessctl pavucontrol mpd zsh doas neovim bemenu papirus-icon-theme arc-gtk3 arc-gtk2 lolcat starship exa ncmpcpp pulseaudio pavucontrol pamixer py3-tldextract pass mako dash gvfs pcmanfm network-manager-applet blueman libindicator py3-pynvim py3-send2trash rust-analyzer swayidle waylock inotify-tools py3-psutil xdg-user-dirs swaylock-effects system-config-printer qemu pigz zathura-pdf-mupdf git neomutt virt-manager texlab engrampa kvantum zsh-syntax-highlighting lynx mpop notmuch newsboat dmenu rofi youtube-dl wireshark torsocks gpg cups clipman wtype wob dunst wev py3-psutil py3-daemon #waybar

apk add font-ubuntu-mono-nerd wlr-randr xfce-polkit tectonic pmbootstrap miniserve nmap texlab urlview abook wdisplays tzdata tor pw-volume


apk add eudev alpine-sdk arc-gtk4
setup-udev
apk add mesa-dri-gallium
apk add river river-doc mandoc firefox
apk add adwaita-icon-theme foot ttf-dejavu
rc-update add seatd
rc-service seatd start

apk del unudhcpd

adduser me
addgroup me audio
addgroup me input
addgroup me seat
addgroup me video
addgroup me wheel
addgroup me abuild

apk add alacritty waybar bottom font-ubuntu-mono-nerd keyd river dbus-x11 toapk py3-daemon bemenu telegram-desktop pciutils-libs cargo rust mesa-vulkan-intel

install -m0644 -t /etc/profile.d/ /root/setup/etc/xdg_runtime_dir.sh


touch /etc/login.defs
mkdir /etc/default
touch /etc/default/useradd

#cargo install --git  https://gitlab.com/snakedye/kile
#netowork
apk add iwd
rc-update del udhcpd
rc-update add dbus
rc-update add iwd

apk del wpa_supplicent

install -m0644 -t /etc/iwd/ /root/setup/etc/main.conf
install -m0644 -t /etc/keyd/ /root/setup/etc/keyd.conf
#nvim /etc/iwd/main.conf

iwctl station wlan0 scan
iwctl station wlan0 get-networks


install -m0644 -t /etc/pipewire/ /root/setup/pipewire.conf
install -m0644 -t /etc/pipewire/ /root/setup/pipewire-pulse.conf

apk add pipewire
apk add wireplumber pipewire-pulse pipewire-alsa
apk add rtkit
adduser me rtkit
modprobe snd_seq
echo snd_seq >> /etc/modules

apk add pipewire-tools
#apk add alsa-utils alsa-utils-doc alsa-lib alsa-conf
#apk add alsa-plugins-pulse

apk add alsa-lib-dev libxkbcommon-dev wayland-dev doas-sudo-shim rust-wasm iwgtk
rc-update add keyd
rc-update add tor
#rc-update add mpd

#rc-update add cups

cp -i /etc/tor/torrc.sample /etc/tor/torrc

mkdir -p /var/cache/distfiles
chmod a+w /var/cache/distfiles

cp /root/setup/user-river.sh /home/me/user-river.sh
chmod 777 /home/me/user-river.sh
chmod +x /home/me/user-river.sh
