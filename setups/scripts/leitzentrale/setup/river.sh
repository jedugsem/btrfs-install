#!/bin/sh
. ./pkg_vars
xbps-install -y -Sy $FINAL
xbps-install -y -S elogind
xbps-rindex -a ./pkgs/*.xbps
xbps-install -Ry ./pkgs/ kile-wl nvim-plug
usermod -aG _seatd me

ln -s /etc/runit/sv/tor /var/service
ln -s /etc/runit/sv/seatd /var/service
#ln -s /etc/runit/sv/keyd /var/service
#ln -s /etc/runit/sv/cups /run/runit/service/
#ln -s /etc/runit/sv/mpd /var/service

#mkdir -p /etc/pipewire
mkdir -p /etc/keyd
mkdir -p /etc/greetd/
mkdir -p /usr/share/wayland-sessions/
#install -m0644 -t /etc/pipewire/ ./confs/etc/pipewire.conf
#install -m0644 -t /etc/pipewire/ ./confs/etc/pipewire-pulse.conf
install -m0644 -t /usr/share/wayland-sessions/ ./confs/river.desktop
install -m0644 -t /etc/greetd/ ./confs/etc/greetd/config.toml
install -m0644 -t /etc/keyd/ ./confs/etc/keyd/keyd.conf
install -m0644 -t /etc/profile.d/ ./confs/etc/profile.d/xdg_runtime_dir.sh
install -m0644 -t /etc/iwd/ ./confs/etc/iwd/main.conf
#install -m0644 -t /etc/profile.d/ ./confs/etc/xdg_runtime_dir.sh
sv restart iwd

#ln -s /etc/runit/sv/ /var/service
#echo "dont forget doas xbps-install void-repo-nonfree intel-ucode"
xbps-install -y -S void-repo-nonfree
xbps-install -y -Sy intel-ucode

#sed -i "s/--noclear/--noclear -a me/" /etc/sv/agetty-tty1/conf
