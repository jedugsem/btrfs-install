#!/bin/bash
echo "nginx{"
pacman -S nginx-mainline rsync certbot-nginx ufw cronie apache #ddclient

#certbot certonly --standalone -d web.baum.uno
#certbot certonly --standalone -d baum.uno
#certbot certonly --standalone -d git.baum.uno
certbot --nginx
echo "put in crontab -e: 0 0 1 * * certbot --nginx renew "
#her backups and other thing could be timed and implemented
crontab -l
echo "user: me \n Password for website: "
htpasswd -c /etc/nginx/myusers me

ufw default deny incoming # block all incoming connections by default
ufw allow in ssh # or: ufw allow in 22
ufw allow 80
ufw allow 443
ufw allow 5000
ufw enable

systemctl enable --now nginx
echo "}nginx"
