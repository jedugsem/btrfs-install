#!/bin/bash
rsync --force -r nginx/ root@servi:/etc/nginx/
rsync --force nginx.sh root@servi:/root/nginx.sh
rsync --force food.sh root@servi:/root/food.sh
rsync --force postgres.sh root@servi:/root/postgres.sh
ssh root@servi "./nginx.sh"
ssh root@servi "./food.sh"
ssh root@servi "./postgres.sh"
ssh root@servi
ssh root@servi "systemctl enable --now postgresql"
echo "}postgres"
