#!/bin/bash
echo "food{"
mkdir -p /var/www/food
ln /etc/nginx/sites-available/food /etc/nginx/sites-enabled/food
ln /etc/nginx/sites-available/server /etc/nginx/sites-enabled/server
systemctl stop nginx
certbot --nginx
systemctl start nginx
echo "}food"
