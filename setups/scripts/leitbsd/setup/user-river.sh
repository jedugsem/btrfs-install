#!/bin/sh
rm -fr .config
rm -f .zshrc
cp ./confs/.profile ~/.profile
echo ".river-dots" >> .gitignore
git clone --bare https://gitlab.com/jedugsem/river-dots $HOME/.river-dots
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME checkout
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME config --local status.showUntrackedFiles no

git clone https://gitlab.com/dwt1/wallpapers $HOME/wallpapers
wal -i wallpapers/0001.jpg

mkdir -p ~/.icons/default
#local
ln -s /usr/share/icons/Adwaita/cursors ~/.icons/default/

#xdg-mime default org.pwmt.zathura.desktop application/pdf



#/etc/rc.conf FONT=/usr/share/kbd/consolefonts

#/etc/rc.local zsh and stuff right before login

#ln -sf /usr/share/zoneinfo/<timezone> /etc/localtime

#export TZ=<timezone>

#The chrony package provides Chrony and the chronyd service

#/etc/gtk-3.0/settings.ini or .config/gtk-3.0/

#cups-pk-helper
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
