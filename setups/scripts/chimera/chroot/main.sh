#!/bin/sh
. /etc/profile
. /home/installer/vars
. /home/installer/chroot/helpers/fns_user.sh
. /home/installer/chroot/helpers/fns_grub.sh
. /home/installer/chroot/helpers/fns_fstab.sh
. /home/installer/chroot/helpers/fns_snapper.sh
. /home/installer/chroot/helpers/fns_locale.sh
. /home/installer/chroot/helpers/fns_swap.sh

#   FSTAB

LUKS_UUID=$(blkid -s UUID -o value /dev/mapper/$CRYPT_NAME)
UUID=$(blkid -s UUID -o value "$DRIVE"2)
EFI_UUID=$(blkid -s UUID -o value "$DRIVE"128)

fstab 

apk update
apk upgrade --available

# Locales

#setup_locales
#sed -i 's/#en_US/en_US/' /etc/locale.gen
#locale-gen
#echo "export LANG=\"en_US.UTF-8\"
#export LC_COLLATE=\"C\"" >> /etc/locale.conf


echo "$SWAP swap"
if $SWAP; then
    # it exports roffset snd 
    create_swap
fi

if $SNAPPER; then
   setup_snapper
fi



setup_grub $UUID $SNAPPER


# Post Installation
echo "Post Installation"
echo $HOSTNAME > /etc/hostname
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

echo "root password"
passwd
useradd $USERNAME
usermod -a -G wheel,kvm $USERNAME
passwd $USERNAME
