#APK="arc-gtk2 arc-gtk3 arc-gtk4 rust rust-wasm pcmanfm py3-psutil py3-daemon py3-tldextract py3-send2trash py3-pywal inotify-tools py3-pynim libindicator font-ubuntu-mono-nerd alpine-sdk mesa-dri-gallium eudev river river-doc mandoc cargo dbus-x11 toapk mesa-vulkan-intel pciutils-libs alsa-lib-dev libxkbcommon-dev wayland-dev doas-sudo-shim pipewire-tools"
#PAC="arc-gtk-theme rustup pcmanfm-gtk3 libindicator-gtk3 nerd-fonts-ubuntu-mono tor-runit kile-wl qt5-wayland python-prctl python-psutil python-inotify-simple python-daemon python-tldextract pywal"

BASE="zsh opendoas neovim zig lolcat-c exa dash xdg-user-dirs pigz git zsh-syntax-highlighting youtube-dl grub-btrfs gcc" 
WAYLAND="swaybg bemenu wev dunst wob wtype clipman swayidle gvfs Waybar wlr-randr swaylock mesa-vulkan-intel seatd river"
CLI="brightnessctl pass torsocks neomutt bottom ncmpcpp tectonic"
SERVICES="cups system-config-printer keyd mpd"
GUI="alacritty qutebrowser papirus-icon-theme zathura imv pavucontrol zathura-pdf-mupdf rofi engrampa xfce-polkit wdisplays telegram-desktop"
QT="kvantum"
DEV="rust-analyzer texlab virt-manager qemu pmbootstrap"
NETWORK="network-manager-applet"
EXTRA="wireshark blueman firefox"
PIPEWIRE="rtkit pipewire alsa-pipewire wireplumber"
VOID="arc-theme rustup pcmanfm libindicator tor qt5-wayland python3-prctl python3-psutil python3-tldextract pywal font-awesome mesa-dri pamixer xtools font-firacode zsh-autosuggestions zsh-completions libspa-bluetooth"
VOID_DEV="libxkbcommon-devel wayland-devel alsa-lib-devel cups-filters"

INSTALL="$BASE $CLI $WAYLAND $SERVICES $CLI $DEV $NETWORK $QT $EXTRA $PIPEWIRE $GUI"

xbps-install -y $INSTALL $VOID $VOID_DEV
xbps-rindex -a /root/*.xbps
xbps-install -Ry /root/ kile-wl nvim-plug
usermod -aG _seatd me

ln -s /etc/runit/sv/tor /var/service
ln -s /etc/runit/sv/seatd /var/service
#ln -s /etc/runit/sv/keyd /var/service
#ln -s /etc/runit/sv/cups /run/runit/service/
#ln -s /etc/runit/sv/mpd /var/service

mkdir -p /etc/pipewire
mkdir -p /etc/keyd
install -m0644 -t /etc/pipewire/ /home/me/setup/etc/pipewire.conf
install -m0644 -t /etc/pipewire/ /home/me/setup/etc/pipewire-pulse.conf
install -m0644 -t /etc/keyd/ /home/me/setup/etc/keyd.conf
install -m0644 -t /etc/profile.d/ /home/me/setup/etc/xdg_runtime_dir.sh

#echo "dont forget doas xbps-install void-repo-nonfree intel-ucode"
xbps-install -y void-repo-nonfree
xbps-install -Sy intel-ucode

sed -i "s/--noclear/--noclear -a me/" /etc/sv/agetty-tty1/conf
