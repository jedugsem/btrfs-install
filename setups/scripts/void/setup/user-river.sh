rm -fr .config
rm -f .zshrc
echo ".river-dots" >> .gitignore
git clone --bare https://gitlab.com/jedugsem/river-dots $HOME/.river-dots
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME checkout
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME config --local status.showUntrackedFiles no

git clone https://gitlab.com/dwt1/wallpapers
wal -i wallpapers/0001.jpg

mkdir -p ~/.icons/default
ln -s /usr/share/icons/Adwaita/cursors .icons/default/cursors

xdg-mime default org.pwmt.zathura.desktop application/pdf

