#!/bin/bash

#apk add cryptsetup btrfs-progs gptfdisk e2fsprogs util-linux dosfstools e2fsprogs-extra zstd mkinitfs

#timedatectl set-ntp true
#source export.sh
DRIVE="/dev/sda"
MOUNT_OPTS="compress=zstd"
#dont change yet
CRYPT="cryptroot"
CRYPT_DEVICE="/dev/mapper/$CRYPT"

ITER="2000"

VOID="base-system cryptsetup grub-x86_64-efi snapper zsh opendoas NetworkManager python3-dbus bc"
echo "gdisk your drive "
echo "Efi (128) and root (2)"


fdisk $DRIVE


cryptsetup luksFormat --type luks1 --iter-time $ITER "$DRIVE"2
#cryptsetup luksFormat --pbkdf pbkdf2 --type luks2 "$DRIVE"2
#cryptsetup luksFormat --type luks2 "$DRIVE"2
cryptsetup open "$DRIVE"2 $CRYPT

umount -Rf /mnt
rm -rf /mnt/*

gdisk -l $DRIVE
mkfs.vfat -F32 "$DRIVE"128
mkfs.btrfs -f -L VERA -n 32k $CRYPT_DEVICE

#mount -o "$MOUNT_OPTS" $CRYPT_DEVICE /mnt/
mount $CRYPT_DEVICE /mnt/

# Create subvolumes automatically with snapper rollback support
# Create first subvolumes

btrfs subvolume create /mnt/@
mkdir /mnt/@/boot
btrfs subvolume create /mnt/@/boot/grub
btrfs subvolume create /mnt/@/.snapshots
mkdir /mnt/@/.snapshots/1
btrfs subvolume create /mnt/@/.snapshots/1/snapshot
btrfs subvolume create /mnt/@/home
btrfs subvolume create /mnt/@/opt
btrfs subvolume create /mnt/@/root
btrfs subvolume create /mnt/@/srv
btrfs subvolume create /mnt/@/tmp
mkdir /mnt/@/usr/
btrfs subvolume create /mnt/@/usr/local
mkdir /mnt/@/var/
btrfs subvolume create /mnt/@/var/cache
btrfs subvolume create /mnt/@/var/log
btrfs subvolume create /mnt/@/var/spool
btrfs subvolume create /mnt/@/var/tmp
btrfs subvolume create /mnt/@/var/lib
btrfs subvolume create /mnt/@/var/swap
chattr +C /mnt/@/var/lib
chattr +C /mnt/@/var/spool
chattr +C /mnt/@/var/cache
chattr +C /mnt/@/var/tmp
chattr +C /mnt/@/var/log
chattr +C /mnt/@/var/swap


echo "<?xml version=\"1.0\"?>
<snapshot>
	<type>single</type>
	<num>1</num>
	<date>$(date +"%Y-%m-%d %H:%M:%S")</date>
	<description>First Snapshot</description>
</snapshot>" > /mnt/@/.snapshots/1/info.xml


#set default subvolume for first snapshot

btrfs subvolume set-default $(btrfs subvolume list /mnt | grep "@/.snapshots/1/snapshot" | sed 's/ID //' | sed 's/ gen.*//') /mnt

umount /mnt

# Mount root and create mount-points 
mount -o "$MOUNT_OPTS" $CRYPT_DEVICE /mnt
mkdir -p /mnt/boot/grub
mkdir /mnt/.snapshots
mkdir /mnt/home
mkdir /mnt/opt
mkdir /mnt/root
mkdir /mnt/srv
mkdir /mnt/tmp
mkdir -p /mnt/usr/local
mkdir /mnt/var
mkdir /mnt/var/spool
mkdir /mnt/var/lib
mkdir /mnt/var/log
mkdir /mnt/var/cache
mkdir /mnt/var/tmp
mkdir /mnt/var/swap

# Mount them in the right directorys
mount -o "$MOUNT_OPTS",subvol=@/.snapshots $CRYPT_DEVICE /mnt/.snapshots 
mount -o "$MOUNT_OPTS",subvol=@/home $CRYPT_DEVICE /mnt/home 
mount -o "$MOUNT_OPTS",subvol=@/opt $CRYPT_DEVICE /mnt/opt
mount -o "$MOUNT_OPTS",subvol=@/root $CRYPT_DEVICE /mnt/root 
mount -o "$MOUNT_OPTS",subvol=@/srv $CRYPT_DEVICE /mnt/srv
mount -o "$MOUNT_OPTS",subvol=@/tmp $CRYPT_DEVICE /mnt/tmp 
mount -o "$MOUNT_OPTS",subvol=@/usr/local $CRYPT_DEVICE /mnt/usr/local
mount -o "$MOUNT_OPTS",subvol=@/var/cache $CRYPT_DEVICE /mnt/var/cache
mount -o "$MOUNT_OPTS",subvol=@/var/log $CRYPT_DEVICE /mnt/var/log
mount -o "$MOUNT_OPTS",subvol=@/var/spool $CRYPT_DEVICE /mnt/var/spool
mount -o "$MOUNT_OPTS",subvol=@/var/lib $CRYPT_DEVICE /mnt/var/lib
mount -o "$MOUNT_OPTS",subvol=@/var/tmp $CRYPT_DEVICE /mnt/var/tmp
mount -o subvol=@/var/swap $CRYPT_DEVICE /mnt/var/swap
mount -o "$MOUNT_OPTS",subvol=@/boot/grub $CRYPT_DEVICE /mnt/boot/grub
		

mkdir -p /mnt/boot/efi/
mount "$DRIVE"128 /mnt/boot/efi

mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/


xbps-install -Sy -R https://repo-default.voidlinux.org/current/musl -r /mnt $VOID


dd bs=512 count=4 if=/dev/random of=/mnt/crypto_keyfile.bin iflag=fullblock
chmod 000 /mnt/crypto_keyfile.bin
cryptsetup -v luksAddKey "$DRIVE"2 /mnt/crypto_keyfile.bin

mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc

cp /etc/resolv.conf /mnt/etc/

cp /root/btrfs-install/void/install-chroot.sh /mnt/
cp /root/btrfs-install/void/btrfs_map_physical /mnt/
cp -r /root/btrfs-install/void/setup/ /mnt/root/
cp /root/btrfs-install/git.sh /mnt/root/setup/
cp /root/btrfs-install/void/*.xbps /mnt/root/
