#!/bin/bash

truncate -s 0 /var/swap/swapfile
chattr +C /var/swap/swapfile
#btrfs property set /var/swap/swapfile compression none
fallocate -l 8G /var/swap/swapfile
chmod 600 /var/swap/swapfile
mkswap /var/swap/swapfile
swapon /var/swap/swapfile

chown root:root /
chmod 755 /
chmod 000 /crypto_keyfile.bin

umount /.snapshots
rm -r /.snapshots
snapper --no-dbus -c root create-config /
btrfs subvolume delete /.snapshots
mkdir /.snapshots
mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/cryptroot /.snapshots
chmod 750 /.snapshots


echo "LANG=en_US.UTF-8" >> /etc/locale.conf
#echo "en_US.UTF-8 UTF-8" >> /etc/default/

echo "win11" > /etc/hostname

LUKS_UUID=$(blkid -s UUID -o value /dev/mapper/cryptroot)
UUID=$(blkid -s UUID -o value /dev/sda2)
EFI_UUID=$(blkid -s UUID -o value /dev/sda128)

echo "#
# See fstab(5).
#
# <file system>	<dir>	<type>	<options>		<dump>	<pass>
tmpfs		/tmp	tmpfs	defaults,nosuid,nodev   0       0
UUID=${LUKS_UUID} / btrfs rw,relatime,compress=zstd:3,ssd,space_cache 0 0
UUID=${LUKS_UUID} /home btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=/@/home 0 0
UUID=${LUKS_UUID} /opt btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=/@/opt 0 0
UUID=${LUKS_UUID} /root btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=/@/root 0 0
UUID=${LUKS_UUID} /srv btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=/@/srv 0 0
UUID=${LUKS_UUID} /tmp btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=/@/tmp 0 0
UUID=${LUKS_UUID} /usr/local btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/usr/local 0 0
UUID=${LUKS_UUID} /var/cache btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/var/cache 0 0
UUID=${LUKS_UUID} /var/log btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/var/log 0 0
UUID=${LUKS_UUID} /var/spool btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/var/spool 0 0
UUID=${LUKS_UUID} /var/lib btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/var/lib 0 0
UUID=${LUKS_UUID} /var/tmp btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/var/tmp 0 0
UUID=${LUKS_UUID} /var/swap btrfs rw,relatime,ssd,space_cache,subvol=@/var/var 0 0
UUID=${LUKS_UUID} /boot/grub btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/boot/grub 0 0
UUID=${LUKS_UUID} /.snapshots btrfs rw,relatime,compress=zstd:3,ssd,space_cache,subvol=@/.snapshots 0 0
UUID=${EFI_UUID} /boot/efi vfat rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro 0 0
/var/swap/swapfile    none    swap    defaults    0   0
" > /etc/fstab

echo "vm.swappiness=10" >> /etc/sysctl.conf

offset=$(/btrfs_map_physical /var/swap/swapfile | head -n2 | awk '/\S/{ s=$NF; } END{ print(s); }')
roffset=$(dc -e ''${offset}' 4096 /p')

echo "cryptroot UUID=${UUID} /crypto_keyfile.bin luks" >> /etc/crypttab

sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux


sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=4\"/GRUB_CMDLINE_LINUX_DEFAULT=\"rd.auto=1 loglevel=4\"/g" /etc/default/grub

echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
echo "GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=${UUID}:cryptroot resume=/dev/mapper/cryptroot resume_offset=${roffset}\"" >> /etc/default/grub
touch /etc/dracut.conf.d/10-crypt.conf
echo "install_items+=\" /crypto_keyfile.bin /etc/crypttab \"" >> /etc/dracut.conf.d/10-crypt.conf

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=boot
cp /boot/efi/EFI/boot/grubx64.efi /boot/efi/EFI/boot/bootx64.efi

#grub-mkconfig -o /boot/grub/grub.cfg

echo root
passwd

echo "permit persist :wheel as root
  permit nopass root as me" >> /etc/doas.conf
xbps-reconfigure -fa

useradd -m me
usermod -aG audio me
usermod -aG input me
usermod -aG video me
usermod -aG network me
#usermod -aG rtkit me
usermod -aG wheel me
echo "me"
passwd me
chsh -s /usr/bin/zsh me
mv /root/setup /home/me

chmod -R 777 /home/me/setup

ln -s /etc/sv/dbus /etc/runit/runsvdir/default
ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default

