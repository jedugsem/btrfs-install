#!/bin/sh 
doas pacman -S rustup
echo "[universe]
Server = https://universe.artixlinux.org/\$arch" | doas tee -a /etc/pacman.conf
doas pacman -Sy artix-archlinux-support
echo "[extra]
Include = /etc/pacman.d/mirrorlist-arch
[community]
Include = /etc/pacman.d/mirrorlist-arch" | doas tee -a /etc/pacman.conf
doas pacman-key --populate archlinux
doas pacman -Sy
rustup default stable
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -s
doas pacman -U paru*.tar.zst
echo "[bin]
FileManager = xplr
Sudo = doas" | doas tee -a /etc/paru.conf
