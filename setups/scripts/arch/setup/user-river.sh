#!/bin/sh

AUDIO="pipewire ncmpcpp blueman mpd pipewire-pulse pavucontrol pamixer"
WAYLAND="wayland wayland-protocols wtype"
RIVER="river kile-wl wob waybar swaylock-effects-git bemenu-wlroots swaybg ttf-font-awesome xorg-xwayland dunst wev"
RIVER_SUITE="zathura alacritty qutebrowser pcmanfm-gtk3 imv network-manager-applet"

CLI="exa git neovim youtube-dl neomutt python-pynvim deno zsh-syntax-highlighting"
BASE="base-devel brightnessctl scdoc pkg-config gvfs starship deno pass opendoas-sudo"
DEV="rustup rust-analyzer"
FONTS=""
BROWSER="qutebrowser qt5-wayland python-prctl python-psutil python-inotify-simple python-daemon python-tldextract"
STUFF="torsocks"
FIXES="keyd-git"
GTK="system-config-printer engrampa virt-manager"


QT="kvantum-qt5 wireshark-qt"
SNAP="snap-pac-grub"
DOTS="neovim-plug python-pynvim deno yofi-git python-pywal xfce-polkit-git arc-gtk-theme papirus-icon-theme nerd-fonts-ubuntu-mono swayidle xdg-user-dirs kvantum qt5ct bottom bluez-utils vulkan-intel texlab skim aarch64-linux-gnu-gcc tectonic mutt-wizard wl-clipboard" 
#AUR="neovim-plug"
#sudo usermod -a -G wireshark me

doas pacman -S openssl rustup git base-devel
rustup toolchain install stable-x86_64-unknown-linux-gnu
rustup target add aarch64-unknown-linux-gnu wasm32-unknown-unknown
cd ~/
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -s
doas pacman -U paru*.tar.zst
cd ~/
rm -rf paru

doas sed -i "s/#\[bin\]/\[bin\]/" /etc/paru.conf
doas sed -i "s/#Sudo/Sudo/" /etc/paru.conf
doas sed -i "s/SudoLoop/#SudoLoop/" /etc/paru.conf

#	minimal
paru -S $AUDIO $CLI $BASE $FONTS $BROWSER $FIXES $WAYLAND $RIVER $RIVER_SUITE $DEV
#	cli
paru -S $DOTS $GTK $QT $FIXES $STUFF 
#	river



doas dd of=/etc/keyd/keyd.conf << EOF
[ids]

*

[main]
f2 = esc
EOF

doas systemctl enable --now keyd

#systemctl --user enable --now mpd
doas systemctl enable tor.service --now
#doas systemctl enable cups --now

rm -rf ~/.river-dots
rm -rf ~/.config
doas rm .zshrc
echo "source ~/.config/zsh/zshrc" > .zshrc
echo ".river-dots" > ~/.gitignore
git clone --bare https://www.gitlab.com/jedugsem/river-dots ~/.river-dots
git --git-dir=/home/me/.river-dots/ --work-tree=/home/me checkout
git --git-dir=/home/me/.river-dots/ --work-tree=/home/me config status.showUntrackedFiles no

nvim -c PlugInstall
#nvim -c "TSInstall rust bash julia toml c latex vim lua"

cd ~/
git clone https://gitlab.com/dwt1/wallpapers

zsh -c "git config --global user.email \"jedugsem@gmail.com\"
git config --global user.name \"jedugsem\""

kvantummanager --set KvArcDark

# not-yet ready
#gpg --full-generate-key
#gpg --list-leys --keyid-format LONG
#gpg --import backupkey.pgp
#gpg --import pub.pgp
#git clone https://gitlab.com/jedugsem/password-store
