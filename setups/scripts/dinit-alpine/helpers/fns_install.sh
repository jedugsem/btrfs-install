#!/bin/sh

installv() {
    DRIVE=$1

    pkgs="alpine-base btrfs-progs e2fsprogs e2fsprogs-extra mkinitfs cryptsetup dosfstools git gptfdisk iw openssl util-linux linux-lts iwd zstd grub grub-efi doas"
    #pkgs="alpine-baselayout alpine-conf alpine-release apk-tools busybox busybox-mdev-openrc busybox-openrc busybox-suid libc-utils"
    apk add --root=/mnt --initdb $(echo $pkgs) --keys-dir /etc/apk/keys --repositories-file /etc/apk/repositories

    echo "http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /mnt/etc/apk/repositories
}
