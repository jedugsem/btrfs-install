#!/bin/bash

echo "
#UNIVERSE
[universe]
Server = https://universe.artixlinux.org/\$arch
Server = https://mirror1.artixlinux.org/universe/\$arch
Server = https://mirror.pascalpuffke.de/artix-universe/\$arch
Server = https://artixlinux.qontinuum.space/artixlinux/universe/os/\$arch
Server = https://mirror1.cl.netactuate.com/artix/universe/\$arch
Server = https://ftp.crifo.org/artix-universe/\$arch" >> /etc/pacman.conf
pacman -Sy artix-archlinux-support
echo "
#ARCH
[extra]
Include = /etc/pacman.d/mirrorlist-arch
[community]
Include = /etc/pacman.d/mirrorlist-arch
" >> /etc/pacman.conf
pacman -Sy
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
sed -i "s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g" /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

echo "KEYMAP=us" >> /etc/vconsole.conf

echo "win11" > /etc/hostname

echo "127.0.0.1 localhost
::1             localhost
127.0.1.1       win11.localdomain       win11" > /etc/hosts


sed -i "s/subvolid=.*,//g" /etc/fstab
sed -i "s/,subvol=.@..snapshots.1.snapshot/    /g" /etc/fstab
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux

umount /.snapshots
rm -r /.snapshots
snapper --no-dbus -c root create-config /
btrfs subvolume delete /.snapshots
mkdir /.snapshots
mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/cryptroot /.snapshots
chmod 750 /.snapshots

# placeholder      $UUID
UUID=$(blkid | grep "/dev/sda2" | sed "s/.* UUID=\"//g" | sed "s/\" TYPE.*//g")

sed -i "s/FILES=()/FILES=(\/crypto_keyfile.bin)/g" /etc/mkinitcpio.conf
sed -i "s/HOOKS=(.*)/HOOKS=(base udev autodetect keyboard keymap modconf block encrypt filesystems)/g" /etc/mkinitcpio.conf

sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=${UUID}:cryptroot\"/g" /etc/default/grub
sed -i "s/loglevel=3 quiet/loglevel=3/g" /etc/default/grub
sed -i "s/#GRUB_ENABLE_CRYPTODISK/GRUB_ENABLE_CRYPTODISK/g" /etc/default/grub


grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=boot
cp /boot/efi/EFI/boot/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
mkinitcpio -p linux
grub-mkconfig -o /boot/grub/grub.cfg

echo root
passwd

echo "permit persist :wheel as root
  permit nopass root as me" >> /etc/doas.d/doas.conf


sed -i "s/#\[bin\]/\[bin\]/" /etc/paru.conf
sed -i "s/#Sudo/Sudo/" /etc/paru.conf
#sed -i "s/SudoLoop/#SudoLoop/" /etc/paru.conf
useradd -m me
usermod -aG audio me
usermod -aG input me
usermod -aG video me
#usermod -aG seat me
#usermod -aG rtkit me
usermod -aG wheel me
echo "me"
passwd me
chsh -s /bin/zsh me
cp -r /root/setup /home/me
chmod -R 777 /home/me/setup
pacman -S rustup
rustup install stable-x86_64-unknown-linux-gnu




