pkgname = "snapper"
pkgver = "0.10.6"
pkgrel = 1
hostmakedepends = []
#makedepends = ['boost', 'lvm2', 'libxslt', 'docbook-xsl', 'git', "linux-pam-devel",]
depends = ["btrfs-progs", "libxml2", "dbus", "boost-libs","acl","json-c"]
pkgdesc = "Snapper"
maintainer = "jedugsem <jedugsem@gmail.com>"
license = "GPL-2.0-or-later"
url = "https://snapper.io"
source = f"https://github.com/openSUSE/snapper/archive/v{pkgver}/{pkgname}-{pkgver}.tar.gz"
sha256 = "f618bb46e0e56c2e2c694715ddd632fb2da48b06028ef6cb62bc68fa83cd898f"
# hardening = ["vis", "!cfi"]


def do_configure(self):
  self.do("echo hey")
  self.do("aclocal")
  self.do("libtoolize","--force","--automake","--copy")
  self.do("autoheader")
  self.do("automake","--add-missing","--copy")
  self.do("autoconf")
  self.do("./configure",
          "--prefix=/usr",
          "--sbindir=/usr/bin",
          "--with-conf=/etc/conf.d",
          "--with-pam-security=/usr/lib/security",
          "--disable-zypp",
          "--disable-silent-rules",)


def do_build(self):
    # primary build
    self.do("make")


def do_install(self):
    ddir = self.chroot_destdir
    # populate extra targets first
    self.do("make",f"DESTDIR={ddir}","install")
    self.install("data/sysconfig.snapper",f"{ddir/etc/conf.d/snapper}")
    self.do("rm -rf","$pkgdir/usr/lib/snapper/testsuite/")


