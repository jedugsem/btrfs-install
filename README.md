=== Scripts for installing riverwm and utils on arch or alpine in a minimal fashion (250MB idle) ===
= Alpine pre setup =
setup-interfaces
rc-service udhcpd start
setup-apkrepos
vi /etc/apk/repository

= Arch pre setup =
iwctl

== Installation ==
- get wifi First
- Alpine enable community repo
- pacman -S git neovim | apk add git neovim


- git clone https://gitlab.com/jedugsem/btrfs-install
- cd btrfs-install
- adjust plate, default: vda
- sh arch/install.sh | sh alpine/install.sh

- arch-chroot /mnt/ | chroot /mnt

- sh install-chroot.sh
- exit
- reboot

- $ sh setup/user-river.sh | # sh setup/river.sh && $ sh user-river.sh

